# docker-compose-web

#### 介绍
docker-compose-web

1、清理无用的image和container

```
docker image prune -f
docker container prune -f
```
2、构建镜像

```
docker-compose build
```

3、启动容器

```
docker-compose up -d
```

